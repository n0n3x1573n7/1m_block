from sys import argv

from blocker import *
from parse_host import Database_set as Database

def main(argv):
    def usage():
        print("python3 1m_block.py <hosts file>")
        print("ex : python3 1m_block.py top-1m.csv")

    try:
        name, hostfile=argv
    except:
        usage()
        exit()

    hosts=Database.get_hosts(hostfile)
    set_iptables()
    netfilter_block(host)

if __name__ == '__main__':
    argv='1m_block.py top-1m.csv'.split()
    main(argv)