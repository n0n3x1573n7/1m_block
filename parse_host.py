import hashlib as hlib

class Database_set:
	def __init__(self):
		self.db=set()

	def add_host(self, host):
		self.db.add(host)

	def __contains__(self, host):
		return host in self.db

	@classmethod
	def yield_hosts(cls, hostfile):
		with open(hostfile, 'r') as f:
			for line in f:
				yield line.strip().split(',')[1]

	@classmethod
	def get_hosts(cls, hostfile):
		db=Database()
		for host in Database.yield_hosts(hostfile):
			db.add_host(host)
		return db

if __name__ == '__main__':
	db=Database_set.get_hosts('top-1m.csv')
	print('google.com' in db)
	print('tweetdeck.com' in db)